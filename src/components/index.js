import ng from 'angular';

import AppHeader from './app-header';
import DetailsInfo from './details-info';
import DetailsCard from './details-card';
import AppMenu from './app-menu';
import AppMap from './app-map';
import AppFooter from './app-footer';
import Overview from './overview';
import TripFeatures from './trip-features';
import TripDescription from './trip-description';
import TripDays from './trip-days';
import DayInfo from './day-info';
import NewContestBlock from './new-contest-block';
import CalendarSlider from './calendar-slider';


export default ng.module('app.components', [AppHeader, DetailsInfo, DetailsCard,
 AppMenu, AppMap, AppFooter, Overview, TripFeatures, TripDescription, TripDays,
  DayInfo, NewContestBlock, CalendarSlider]).name;
