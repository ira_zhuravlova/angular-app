import template from './new-contest-block-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'newContestBlockCtrl'

};
