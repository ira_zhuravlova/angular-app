import ng from 'angular';

import NewContestBlockComponent from './components';

export default ng.module('app.components.newContestBlock', ['ui.bootstrap'])
    .component('newContestBlock', NewContestBlockComponent)
    .name;
