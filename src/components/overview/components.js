import template from './overview-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'overviewCtrl',
  bindings:{
    isOverviewPage: '='
  }

};
