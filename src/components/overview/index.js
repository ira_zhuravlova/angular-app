import ng from 'angular';

import OverviewComponent from './components';

export default ng.module('app.components.overview', ['ui.bootstrap'])
    .component('overview', OverviewComponent)
    .name;
