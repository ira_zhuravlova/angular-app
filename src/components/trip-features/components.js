import template from './trip-features-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'tripFeaturesCtrl'

};
