import ng from 'angular';

import TripFeaturesComponent from './components';

export default ng.module('app.components.tripFeatures', ['ui.bootstrap'])
    .component('tripFeatures', TripFeaturesComponent)
    .name;
