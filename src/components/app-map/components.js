import template from './app-map-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'appMapCtrl',
  bindings:{
    isMenuOpen: '=',
    isMapOpen: '='
  }

};
