import ng from 'angular';

import AppMapComponent from './components';

export default ng.module('app.components.appMap', ['ui.bootstrap'])
    .component('appMap', AppMapComponent)
    .name;
