import ng from 'angular';

import DetailsInfoComponent from './components';

export default ng.module('app.components.detailsInfo', [])
    .component('detailsInfo', DetailsInfoComponent)
    .name;
