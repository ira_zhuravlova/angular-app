import template from './details-info-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'detailsInfoCtrl',
    bindings: {
    isMenuOpen: '=',
    isMapOpen: '='
  }

};
