import template from './app-menu-template.html.hamlc';
import controller from './controllers';

export default {
  template,
  controller,
  restrict: 'E',
  controllerAs: 'appMenuCtrl',
   bindings: {
    isMenuOpen: '='
  }
};
