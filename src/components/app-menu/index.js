import ng from 'angular';

import AppMenuComponent from './components';

export default ng.module('app.components.appMenu', [])
    .component('appMenu', AppMenuComponent)
    .name;
