import template from './app-footer-template.html.hamlc';
import controller from './controllers';

export default {
  template,
  controller,
  restrict: 'E',
  controllerAs: 'appFooterCtrl',
   bindings: {
    isOverviewPage: '='
  }
};
