import ng from 'angular';

import AppFooterComponent from './components';

export default ng.module('app.components.appFooter', ['ui.bootstrap'])
    .component('appFooter', AppFooterComponent)
    .name;
