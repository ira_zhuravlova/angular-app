import ng from 'angular';

import TripDescriptionComponent from './components';

export default ng.module('app.components.tripDescription', ['ui.bootstrap'])
    .component('tripDescription', TripDescriptionComponent)
    .name;
