import template from './trip-description-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'tripDescriptionCtrl'

};
