import template from './calendar-slider-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'calendarSliderCtrl'

};
