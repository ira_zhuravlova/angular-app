import ng from 'angular';

import CalendarSliderComponent from './components';

export default ng.module('app.components.calendarSlider', ['ui.bootstrap'])
    .component('calendarSlider', CalendarSliderComponent)
    .name;
