import ng from 'angular';

import DayInfoComponent from './components';

export default ng.module('app.components.dayInfo', ['ui.bootstrap'])
    .component('dayInfo', DayInfoComponent)
    .name;
