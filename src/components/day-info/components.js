import template from './day-info-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'dayInfoCtrl',
  bindings:{
    dayNumber: '<'
  }

};
