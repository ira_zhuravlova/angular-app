import ng from 'angular';

import TripDaysComponent from './components';

export default ng.module('app.components.tripDays', ['ui.bootstrap'])
    .component('tripDays', TripDaysComponent)
    .name;
