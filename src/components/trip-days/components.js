import template from './trip-days-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'tripDaysCtrl'
  
};
