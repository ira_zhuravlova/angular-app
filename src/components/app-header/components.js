import template from './app-header-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'appHeaderCtrl',
  bindings: {
    isMenuOpen: '=',
    isOverviewPage: '='
  }

};
