import ng from 'angular';
import AppHeaderComponent from './components';

export default ng.module('app.components.appHeader', [])
    .component('appHeader', AppHeaderComponent)
    .name;
