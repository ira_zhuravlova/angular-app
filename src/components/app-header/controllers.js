import ng from 'angular';

export default class AppHeaderController {
  constructor() {
    'ngInject';

  }

  $onInit() {

  }

  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
  }

}