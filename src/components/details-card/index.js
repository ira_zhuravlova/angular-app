import ng from 'angular';

import DetailsCardComponent from './components';

export default ng.module('app.components.detailsCard', ['ui.bootstrap'])
    .component('detailsCard', DetailsCardComponent)
    .name;
