import template from './details-card-template.html.hamlc';
import controller from './controllers';

export default {

  template,
  controller,
  restrict: 'E',
  controllerAs: 'detailsCardCtrl'
  
};
