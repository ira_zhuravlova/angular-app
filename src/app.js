import ng from 'angular';

import './style.scss';
import 'angular-ui-bootstrap';
import ngRoute from 'angular-route';

import Components from './components';

ng.module('app', [Components, 'ui.bootstrap', ngRoute])
  .config(['$routeProvider',
    function($routeProvider) {
      $routeProvider
        .when('/overview', {
          name: 'overview',
          template: '<overview is-overview-page="root.isOverviewPage"></overview>'
        })
        .when('/details', {
          name: 'details',
          template: '<details-info is-map-open="root.isMapOpen" is-menu-open="root.isMenuOpen"></details-info> <app-map is-menu-open="root.isMenuOpen" is-map-open="root.isMapOpen"></app-map>'
        })
        .otherwise({
          redirectTo: '/overview'
        });
    }
  ])
  .controller('RootController', ['$scope',
    function($scope) {
      this.$scope = $scope;
      this.isMenuOpen = false;
      this.isMapOpen = false;

      this.$scope.$on("$routeChangeSuccess", (event, current, previous) => {

        this.routeName = current.$$route.name;

        if (this.routeName === 'overview') {
          this.isOverviewPage = true;

        } else {
          this.isOverviewPage = false;
        }

      });
    }
  ]);